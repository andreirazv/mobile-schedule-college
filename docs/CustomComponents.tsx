import React from 'react';
import {Alert, View} from 'react-native';
import ScButton from '../src/components/sc-button/ScButton';
import ScTextInput from '../src/components/sc-text-input/ScTextInput';

interface ICustomComponentsState {
  username: string;
  bgColor: string;
}

export default class CustomComponents extends React.Component<
  any,
  ICustomComponentsState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      username: '',
      bgColor: 'white',
    };
  }

  private onPressBtn() {
    Alert.alert('username', this.state.username);
    this.setState({
      bgColor: 'green',
    });
  }

  private onChangeUserName = (changedText: string) => {
    this.setState({
      username: changedText,
      bgColor: 'white',
    });
  };

  render() {
    return (
      <View>
        <ScButton text="Login in" onClick={this.onPressBtn.bind(this)} />
        <ScButton
          text="register"
          onClick={this.onPressBtn.bind(this)}
          color="red"
        />
        <ScTextInput
          value={this.state.username}
          icon="android"
          bgColor={this.state.bgColor}
          placeHolder="Username"
          onChangeText={this.onChangeUserName.bind(this)}
        />
      </View>
    );
  }
}
