import axios from 'axios';
import {config} from '../src/config/config';

interface IExample {
  id: number;
  username: string;
}

export default class ExampleService {
  public static delete = async (id: string): Promise<any> => {
    const result = await axios.delete(`${config.apiUrl}/example/${id}`);
    return result.data;
  };

  public static getAll = async (): Promise<IExample[]> => {
    const result = await axios.get(`${config.apiUrl}/example`);
    return result.data;
  };

  public static getById = async (id: string): Promise<IExample> => {
    const result = await axios.get(`${config.apiUrl}/example/id/${id}`);
    return result.data;
  };

  public static insert = async (example: IExample): Promise<IExample> => {
    const result = await axios.post(`${config.apiUrl}/example`, example);
    return result.data;
  };

  public static update = async (
    id: number,
    example: IExample,
  ): Promise<IExample> => {
    const result = await axios.put(`${config.apiUrl}/example/${id}`, example);
    return result.data;
  };
}
