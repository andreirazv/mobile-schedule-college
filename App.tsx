import React from 'react';
import {Root} from 'native-base';
import Routes from './src/components//routes/route';
import TabViewActivity from './src/components/add-activity/AddActivity';

export default class App extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }

  render() {
    console.disableYellowBox = true;
    return (
      <Root>
        <Routes />
      </Root>
    );
  }
}
