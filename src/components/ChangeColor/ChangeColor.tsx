import React, {Fragment} from 'react';
import {View, Container, Text} from 'native-base';
import {StyleSheet} from 'react-native';
import SwitchSelector from 'react-native-switch-selector';

import ScButton from '../sc-button/ScButton';
import LanguageService from '../../services/LanguageService';
import {DidacticActivityColor} from '../../data-models/DidacticActivityColor';
import DidacticActivityService from '../../services/DidacticActivityService';
import IDidacticActivity from '../../data-models/DidacticActivity';
import Snackbar from 'react-native-snackbar';
import CustomHeader from '../navigation-menu/custom-header/CustomHeader';
import stylesHeader from '../navigation-menu/styles/NavMenuStyle';
import {AxiosError} from 'axios';
import stylesNav from '../navigation-menu/styles/NavMenuStyle';

import {DidacticActivityStatus} from '../../data-models/DidacticActivityStatus';

interface IStateChangeColor {
  languageService: LanguageService;
  didacticActivityColor: DidacticActivityColor;
  didacticItem: IDidacticActivity;
}

export default class ChangeColor extends React.Component<
  any,
  IStateChangeColor
> {
  constructor(props: any) {
    super(props);
    this.state = {
      didacticItem: Object(null),
      languageService: new LanguageService(),
      didacticActivityColor: Object(null),
    };
  }

  getDidacticItem() {
    const {params} = this.props.navigation.state;
    this.setState({
      didacticItem: params ? params.didacticItem : null,
      didacticActivityColor: params
        ? params.didacticItem.color
        : DidacticActivityColor.GREEN,
    });
  }
  componentDidMount() {
    this.getDidacticItem();
  }

  async handleSend() {
    this.state.didacticItem.color = this.state.didacticActivityColor;
    switch (this.state.didacticActivityColor) {
      case DidacticActivityColor.RED:
        this.state.didacticItem.status = DidacticActivityStatus.NO_CHANGES;
        break;
      case DidacticActivityColor.GREEN:
        this.state.didacticItem.status = DidacticActivityStatus.ALL_CHANGES;
        break;
      case DidacticActivityColor.YELLOW:
        this.state.didacticItem.status = DidacticActivityStatus.SOME_CHANGES;
        break;
    }
    this.getInitial();
    DidacticActivityService.updateActivity(this.state.didacticItem)
      .then(item => {
        this.showSnackBarMessage('Activity was changed!');
        this.props.navigation.state.params.onGoBack();
        this.props.navigation.goBack();
      })
      .catch((er: AxiosError) => {
        this.showSnackBarMessage(
          this.state.languageService.get('general_error'),
        );
      });
  }

  private showSnackBarMessage(message: string) {
    Snackbar.show({
      title: message,
      duration: Snackbar.LENGTH_LONG,
    });
  }

  private getInitial(): number {
    switch (this.state.didacticActivityColor) {
      case DidacticActivityColor.RED:
        return 2;
      case DidacticActivityColor.GREEN:
        return 0;
      case DidacticActivityColor.YELLOW:
        return 1;
    }
  }

  render() {
    return (
      <Container>
        {this.state.didacticItem.id != null && (
          <Fragment>
            <CustomHeader navigation={this.props.navigation} titleText="home">
              <View style={stylesHeader.customHeader}>
                <Text style={[stylesNav.textFormat, stylesNav.subTitleNavMenu]}>
                  Change activity
                </Text>
              </View>
            </CustomHeader>
            <SwitchSelector
              initial={this.getInitial()}
              onPress={(value: DidacticActivityColor) =>
                this.setState({didacticActivityColor: value})
              }
              style={{
                width: 200,
                margin: 20,
                alignSelf: 'center',
              }}
              textColor={'white'}
              selectedColor={'white'}
              buttonColor={'grey'}
              borderColor={'white'}
              backgroundColor={'#719192'}
              height={50}
              hasPadding
              options={[
                {
                  label: 'Allow',
                  value: DidacticActivityColor.GREEN,
                  activeColor: 'green',
                },
                {
                  label: 'Maybe',
                  value: DidacticActivityColor.YELLOW,
                  activeColor: 'orange',
                },
                {
                  label: 'Deny',
                  value: DidacticActivityColor.RED,
                  activeColor: 'red',
                },
              ]}
            />
            <View style={styles.sendButton}>
              <ScButton
                onClick={this.handleSend.bind(this)}
                text={this.state.languageService.get('send')}
              />
            </View>
            <View style={{marginTop: 50}}>
              <Text
                style={[styles.textFormat, {fontSize: 20, fontWeight: 'bold'}]}>
                Info
              </Text>
              <Text style={styles.textFormat}>Always exchange (green)</Text>
              <Text style={styles.textFormat}>
                Exchange only with other students (yellow)
              </Text>
              <Text style={styles.textFormat}>Can't exchange (red)</Text>
            </View>
          </Fragment>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  textFormat: {
    textAlign: 'center',
  },
  titleView: {
    alignItems: 'center',
    backgroundColor: '#719192',
  },
  titleText: {
    color: '#FFFFFF',
    fontSize: 24,
    textAlign: 'center',
    padding: 20,
  },
  modalContent: {
    marginTop: 100,
  },
  sendButton: {
    alignItems: 'center',
    marginTop: 20,
  },
  header: {
    height: 50,
    backgroundColor: '#719192',
  },
});
