import * as React from 'react';
import {StyleSheet, TouchableHighlight} from 'react-native';
import {View, Text} from 'native-base';
import IDidacticActivity from '../../../data-models/DidacticActivity';
import {Fragment} from 'react';
import ITeacherActivity from '../../../data-models/TeacherActivity';
import {IUser} from '../../../data-models/User';
import AsyncStorage from '@react-native-community/async-storage';
import {UserType} from '../../../data-models/UserType';
import {DidacticActivityColor} from '../../../data-models/DidacticActivityColor';
import {DidacticActivityType} from '../../../data-models/DidacticActivityType';

interface IMainViewProps {
  didacticActivity: IDidacticActivity & ITeacherActivity;
  navigation: any;
  onRefresh(): void;
}
export default class EventDay extends React.Component<IMainViewProps, any> {
  constructor(props: IMainViewProps) {
    super(props);
  }

  private deleteSecoundsFromTime(time: string): string {
    return time.slice(0, -3);
  }

  private getEndDate(time: string): string {
    const startTime = this.deleteSecoundsFromTime(time);
    const newStartTime = Number.parseInt(startTime.slice(0, 2));
    return newStartTime + this.props.didacticActivity.duration + ':00';
  }

  private refresh() {
    this.props.onRefresh();
    this.setState({});
  }

  private async openRequest() {
    if (this.props.didacticActivity.type != DidacticActivityType.COURSE) {
      const jsonUser = await AsyncStorage.getItem('logged_user');
      if (jsonUser != null) {
        const user: IUser = JSON.parse(jsonUser) as IUser;
        if (user != null) {
          switch (user.roles[0].type) {
            default:
              throw new Error('Unknown type');
            case UserType.STUDENT:
              if (
                this.props.didacticActivity.color != DidacticActivityColor.RED
              ) {
                this.props.navigation.navigate('RequestView', {
                  didacticItem: this.props.didacticActivity,
                  onGoBack: () => this.refresh(),
                });
              }
              break;
            case UserType.TEACHER:
              this.props.navigation.navigate('ChangeColor', {
                didacticItem: this.props.didacticActivity,
                onGoBack: () => this.refresh(),
              });
              break;
            case UserType.ADMINISTRATOR:
              throw new Error('ADMIN is not implemented2');
          }
        }
      }
    }
  }

  private getColor(): string {
    if (this.props.didacticActivity.type == DidacticActivityType.COURSE)
      return '#719192';
    else
      switch (this.props.didacticActivity.color) {
        case DidacticActivityColor.GREEN:
          return 'green';
        case DidacticActivityColor.YELLOW:
          return 'orange';
        case DidacticActivityColor.RED:
          return 'red';
      }
  }

  render() {
    return (
      <TouchableHighlight
        style={styles.container}
        underlayColor={'#3c4245'}
        onPress={() => this.openRequest()}>
        <Fragment>
          <View
            style={[{backgroundColor: this.getColor()}, styles.didacticStatus]}
          />
          <View style={styles.rowAlign}>
            <Text>
              {this.deleteSecoundsFromTime(this.props.didacticActivity.time)}
            </Text>
            <Text style={[styles.wrapText, styles.didacticTitle]}>
              {this.props.didacticActivity.courseName}
            </Text>
          </View>
          <View style={styles.rowAlign}>
            <Text>{this.getEndDate(this.props.didacticActivity.time)}</Text>
            <Text style={styles.wrapText}>
              {this.props.didacticActivity.type}
            </Text>
            <Text style={styles.wrapText}>
              {this.props.didacticActivity.room}
            </Text>
            {this.props.didacticActivity.tutor && (
              <Text style={styles.wrapText}>
                {this.props.didacticActivity.tutor.firstName +
                  ' ' +
                  this.props.didacticActivity.tutor.lastName}
              </Text>
            )}
          </View>
          <View style={styles.permissionItem}></View>
        </Fragment>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  permissionItem: {
    position: 'absolute',
    alignSelf: 'flex-end',
  },
  container: {
    backgroundColor: '#F8F8FF',
    alignSelf: 'stretch',
    textAlign: 'center',
    opacity: 0.8,
    padding: 20,
    paddingLeft: 0,
    marginBottom: 10,
  },
  didacticStatus: {
    width: 10,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  wrapText: {
    flexWrap: 'wrap',
    flex: 1,
    marginLeft: 20,
  },
  rowAlign: {
    flexDirection: 'row',
    paddingLeft: 20,
  },
  didacticTitle: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});
