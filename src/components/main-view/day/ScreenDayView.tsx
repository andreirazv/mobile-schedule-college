import React, {Component} from 'react';
import {Dimensions, StyleSheet, View, RefreshControl} from 'react-native';

import EventDay from './EventDay';
import {ScrollView} from 'react-native-gesture-handler';
const SCREEN_WIDTH = Dimensions.get('window').width;

interface IPropsMainView {
  didacticActivities: any[] | any;
  navigation: any;
  onRefresh(): void;
}
interface IStateScreenDayView {
  refreshing: boolean;
}
export default class ScreenDayView extends Component<
  IPropsMainView,
  IStateScreenDayView
> {
  constructor(props: IPropsMainView) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  _onRefresh() {
    this.setState({refreshing: true});
    this.props.onRefresh();
    this.setState({refreshing: false});
  }

  render() {
    return (
      <View style={[styles.screen, styles.scrollPage]}>
        {this.props.didacticActivities.length > 0 && (
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh.bind(this)}
              />
            }>
            {this.props.didacticActivities.map((item: any) => {
              return (
                <EventDay
                  onRefresh={() => this.props.onRefresh()}
                  navigation={this.props.navigation}
                  didacticActivity={item}
                />
              );
            })}
          </ScrollView>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    flexDirection: 'row',
  },
  scrollPage: {
    width: SCREEN_WIDTH,
    padding: 10,
  },
  screen: {
    backgroundColor: 'white',
  },
  text: {
    fontSize: 30,
    fontWeight: 'bold',
  },
});
