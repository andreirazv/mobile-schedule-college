import * as React from 'react';
import {Dimensions} from 'react-native';
import {SceneMap, TabView, TabBar} from 'react-native-tab-view';
import ScreenDayView from './ScreenDayView';
import IDidacticActivity from '../../../data-models/DidacticActivity';
import ITeacherActivity from '../../../data-models/TeacherActivity';
import {WeekType} from '../../../data-models/WeekType';

interface IDayViewState {
  index: number;
  routes: any[];
}

interface IDayViewProps {
  mondayActivities: IDidacticActivity[] & ITeacherActivity[];
  tuesdayActivities: IDidacticActivity[] & ITeacherActivity[];
  wednesdayActivities: IDidacticActivity[] & ITeacherActivity[];
  thursdayActivities: IDidacticActivity[] & ITeacherActivity[];
  fridayActivities: IDidacticActivity[] & ITeacherActivity[];
  saturdayActivities: IDidacticActivity[] & ITeacherActivity[];
  sundayActivities: IDidacticActivity[] & ITeacherActivity[];
  navigation: any;
  onRefresh(): void;
  weekType: WeekType;
}

export default class DayView extends React.Component<
  IDayViewProps,
  IDayViewState
> {
  constructor(props: IDayViewProps) {
    super(props);
    this.state = {
      index: this.getIndexDay(),
      routes: [
        {key: 'Monday', title: 'Monday'},
        {key: 'Tuesday', title: 'Tuesday'},
        {key: 'Wednesday', title: 'Wednesday'},
        {key: 'Thursday', title: 'Thursday'},
        {key: 'Friday', title: 'Friday'},
      ],
    };

    if (this.props.saturdayActivities.length > 0) {
      this.state.routes.push({key: 'Saturday', title: 'Saturday'});
    }

    if (this.props.sundayActivities.length > 0) {
      this.state.routes.unshift({key: 'Sunday', title: 'Sunday'});
    }
  }

  private getIndexDay(): number {
    let day = new Date().getDay();
    if (!this.props.sundayActivities.length) day--;

    return day;
  }

  private goToToday() {
    this.setState({
      index: this.getIndexDay(),
    });
  }

  private getScreenDayView(activities: any[]) {
    return (
      <ScreenDayView
        onRefresh={() => this.props.onRefresh()}
        navigation={this.props.navigation}
        didacticActivities={
          activities.filter(x => x.weekType === this.props.weekType) || []
        }
      />
    );
  }

  render() {
    return (
      <TabView
        renderTabBar={renderTabBar}
        tabBarPosition={'bottom'}
        navigationState={this.state}
        renderScene={SceneMap({
          Monday: () => this.getScreenDayView(this.props.mondayActivities),
          Tuesday: () => this.getScreenDayView(this.props.tuesdayActivities),
          Wednesday: () =>
            this.getScreenDayView(this.props.wednesdayActivities),
          Thursday: () => this.getScreenDayView(this.props.thursdayActivities),
          Friday: () => this.getScreenDayView(this.props.fridayActivities),
          Saturday: () => this.getScreenDayView(this.props.saturdayActivities),
          Sunday: () => this.getScreenDayView(this.props.sundayActivities),
        })}
        onIndexChange={index => this.setState({index: index})}
        initialLayout={{width: Dimensions.get('window').width}}
      />
    );
  }
}
const renderTabBar = (props: any) => (
  <TabBar
    {...props}
    indicatorStyle={{backgroundColor: 'white'}}
    style={{backgroundColor: '#719192'}}
  />
);
