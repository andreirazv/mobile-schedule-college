import * as React from 'react';
import {Container, View} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import {Fragment} from 'react';
import {Icon} from 'react-native-elements';
import SwitchSelector from 'react-native-switch-selector';

import CustomHeader from '../navigation-menu/custom-header/CustomHeader';
import ScPicker from '../sc-picker/ScPicker';
import DayView from './day/DayView';
import WeekView from './week/WeekView';
import IDidacticActivity from '../../data-models/DidacticActivity';
import DidacticActivityService from '../../services/DidacticActivityService';
import {IUser} from '../../data-models/User';
import ActivityIndicator from '../activity-indicator/ActivityIndicator';
import styles from '../navigation-menu/styles/NavMenuStyle';
import ITeacherActivity from '../../data-models/TeacherActivity';
import {WeekType} from '../../data-models/WeekType';
import {DayOfTheWeek} from '../../data-models/DayOfTheWeek';

interface IMainViewState {
  isLoaded: boolean;
  currentType: string;
  mondayActivities: IDidacticActivity[] & ITeacherActivity[];
  tuesdayActivities: IDidacticActivity[] & ITeacherActivity[];
  wednesdayActivities: IDidacticActivity[] & ITeacherActivity[];
  thursdayActivities: IDidacticActivity[] & ITeacherActivity[];
  fridayActivities: IDidacticActivity[] & ITeacherActivity[];
  saturdayActivities: IDidacticActivity[] & ITeacherActivity[];
  sundayActivities: IDidacticActivity[] & ITeacherActivity[];
  weekType: WeekType;
}
export default class MainView extends React.Component<any, IMainViewState> {
  constructor(props: any) {
    super(props);
    this.state = {
      isLoaded: false,
      currentType: 'Days',
      mondayActivities: [],
      tuesdayActivities: [],
      wednesdayActivities: [],
      thursdayActivities: [],
      fridayActivities: [],
      saturdayActivities: [],
      sundayActivities: [],
      weekType: WeekType.ODD,
    };
  }

  private onChangeScheduleType = (changedType: string) => {
    this.setState({
      currentType: changedType,
    });
  };

  private findActivitiesByDay(
    dayOfTheWeek: DayOfTheWeek,
    didacticActivities: any,
  ): IDidacticActivity[] & ITeacherActivity[] {
    return didacticActivities.filter((activity: any) => {
      if (activity.dayOfTheWeek === dayOfTheWeek) return activity;
    });
  }

  componentDidMount() {
    this.loadActivities();
  }

  private async loadActivities() {
    const jsonUser = await AsyncStorage.getItem('logged_user');
    if (jsonUser != null) {
      const user: IUser = JSON.parse(jsonUser) as IUser;
      if (user != null) {
        DidacticActivityService.getActivities(user).then(
          (returnedDidacticActivities: any) => {
            this.setState({
              sundayActivities: this.findActivitiesByDay(
                DayOfTheWeek.SUNDAY,
                returnedDidacticActivities,
              ),
              mondayActivities: this.findActivitiesByDay(
                DayOfTheWeek.MONDAY,
                returnedDidacticActivities,
              ),
              tuesdayActivities: this.findActivitiesByDay(
                DayOfTheWeek.TUESDAY,
                returnedDidacticActivities,
              ),
              wednesdayActivities: this.findActivitiesByDay(
                DayOfTheWeek.WEDNESDAY,
                returnedDidacticActivities,
              ),
              thursdayActivities: this.findActivitiesByDay(
                DayOfTheWeek.THURSDAY,
                returnedDidacticActivities,
              ),
              fridayActivities: this.findActivitiesByDay(
                DayOfTheWeek.FRIDAY,
                returnedDidacticActivities,
              ),
              saturdayActivities: this.findActivitiesByDay(
                DayOfTheWeek.SATURDAY,
                returnedDidacticActivities,
              ),
              isLoaded: true,
            });
          },
        );
      }
    }
  }

  render() {
    return (
      <Container>
        <CustomHeader navigation={this.props.navigation} titleText="home">
          <View style={styles.customHeader}>
            <ScPicker
              selectedValue={this.state.currentType}
              colorText={'white'}
              onChangeValue={(itemValue: string) =>
                this.onChangeScheduleType(itemValue)
              }
              pickerItems={['Days', 'Weeks']}
            />
            <SwitchSelector
              initial={0}
              onPress={(value: WeekType) => this.setState({weekType: value})}
              style={{width: 80, alignItems: 'center'}}
              textColor={'white'}
              selectedColor={'white'}
              buttonColor={'#a9bdbd'}
              backgroundColor={'#4f6566'}
              height={30}
              options={[
                {label: 'W1', value: WeekType.ODD},
                {label: 'W2', value: WeekType.EVEN},
              ]}
            />
            {/* <Icon
              onPress={() => this.goToToday()}
              style={{}}
              name={'today'}
              size={25}
              color="#ffffff"
            /> */}
          </View>
        </CustomHeader>
        {!this.state.isLoaded && <ActivityIndicator />}
        {this.state.isLoaded && (
          <Fragment>
            {this.state.currentType === 'Days' && (
              <DayView
                onRefresh={() => this.loadActivities()}
                weekType={this.state.weekType}
                navigation={this.props.navigation}
                sundayActivities={this.state.sundayActivities}
                mondayActivities={this.state.mondayActivities}
                tuesdayActivities={this.state.tuesdayActivities}
                wednesdayActivities={this.state.wednesdayActivities}
                thursdayActivities={this.state.thursdayActivities}
                fridayActivities={this.state.fridayActivities}
                saturdayActivities={this.state.saturdayActivities}
              />
            )}
            {this.state.currentType === 'Weeks' && (
              <WeekView
                onRefresh={() => this.loadActivities()}
                weekType={this.state.weekType}
                navigation={this.props.navigation}
                sundayActivities={this.state.sundayActivities}
                mondayActivities={this.state.mondayActivities}
                tuesdayActivities={this.state.tuesdayActivities}
                wednesdayActivities={this.state.wednesdayActivities}
                thursdayActivities={this.state.thursdayActivities}
                fridayActivities={this.state.fridayActivities}
                saturdayActivities={this.state.saturdayActivities}
              />
            )}
          </Fragment>
        )}
      </Container>
    );
  }
  private goToToday(): void {
    throw new Error('Method not implemented.');
  }
}
