import * as React from 'react';
import {
  StyleSheet,
  Text,
  Dimensions,
  TouchableHighlight,
  RefreshControl,
} from 'react-native';
import {View} from 'native-base';
import {ScrollView} from 'react-native-gesture-handler';
import IDidacticActivity from '../../../data-models/DidacticActivity';
import WeekViewItem from './WeekViewItem';
import {DidacticActivityType} from '../../../data-models/DidacticActivityType';
import ITeacherActivity from '../../../data-models/TeacherActivity';
import AsyncStorage from '@react-native-community/async-storage';
import {IUser} from '../../../data-models/User';
import {WeekType} from '../../../data-models/WeekType';
import {UserType} from '../../../data-models/UserType';
import {DidacticActivityColor} from '../../../data-models/DidacticActivityColor';

interface IWeekViewState {
  days: string[];
  hours: string[];
  refreshing: boolean;
}

interface IDayViewProps {
  navigation: any;
  mondayActivities: IDidacticActivity[] & ITeacherActivity[];
  tuesdayActivities: IDidacticActivity[] & ITeacherActivity[];
  wednesdayActivities: IDidacticActivity[] & ITeacherActivity[];
  thursdayActivities: IDidacticActivity[] & ITeacherActivity[];
  fridayActivities: IDidacticActivity[] & ITeacherActivity[];
  saturdayActivities: IDidacticActivity[] & ITeacherActivity[];
  sundayActivities: IDidacticActivity[] & ITeacherActivity[];
  weekType: WeekType;
  onRefresh(): void;
}
const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default class WeekView extends React.Component<
  IDayViewProps,
  IWeekViewState
> {
  constructor(props: IDayViewProps) {
    super(props);
    this.state = {
      refreshing: false,
      days: ['lun.', 'mar.', 'mie.', 'joi', 'vin.'],
      hours: [],
    };
    if (this.props.sundayActivities.length > 0) {
      this.state.days.unshift('dum.');
    }
    if (this.props.saturdayActivities.length > 0) {
      this.state.days.push('sam.');
    }
  }

  private goToWeek() {
    return 0; //todo go to par/impar week
  }

  private getActivityByPosition(
    collumn: number,
    row: number,
  ): IDidacticActivity & ITeacherActivity {
    let didacticList: any[] = [];
    let newColl = collumn;
    if (!this.props.sundayActivities.length) {
      newColl++;
    }
    switch (newColl) {
      case 0:
        didacticList =
          this.props.sundayActivities.filter(
            x => x.weekType === this.props.weekType,
          ) || [];
        break;
      case 1:
        didacticList =
          this.props.mondayActivities.filter(
            x => x.weekType === this.props.weekType,
          ) || [];
        break;
      case 2:
        didacticList =
          this.props.tuesdayActivities.filter(
            x => x.weekType === this.props.weekType,
          ) || [];
        break;
      case 3:
        didacticList =
          this.props.wednesdayActivities.filter(
            x => x.weekType === this.props.weekType,
          ) || [];
        break;
      case 4:
        didacticList =
          this.props.thursdayActivities.filter(
            x => x.weekType === this.props.weekType,
          ) || [];
        break;
      case 5:
        didacticList =
          this.props.fridayActivities.filter(
            x => x.weekType === this.props.weekType,
          ) || [];
        break;
      case 6:
        didacticList =
          this.props.saturdayActivities.filter(
            x => x.weekType === this.props.weekType,
          ) || [];
        break;
    }
    return (
      didacticList.find((item: IDidacticActivity | ITeacherActivity) => {
        if (row === (Number.parseInt(item.time.slice(0, 2)) - 8) / 2) {
          return item;
        }
      }) || Object(null)
    );
  }

  private getColorByType(
    color: DidacticActivityColor,
    type: DidacticActivityType,
  ) {
    if (color && type) {
      if (type == DidacticActivityType.COURSE) return '#719192';
      else
        switch (color) {
          case DidacticActivityColor.GREEN:
            return 'green';
          case DidacticActivityColor.RED:
            return 'red';
          case DidacticActivityColor.YELLOW:
            return 'orange';
          default:
            return '';
        }
    }
  }
  private refresh() {
    this.props.onRefresh();
    this.setState({});
  }

  private async openRequest(
    didacticActivity: IDidacticActivity & ITeacherActivity,
  ) {
    if (didacticActivity.type != DidacticActivityType.COURSE) {
      const jsonUser = await AsyncStorage.getItem('logged_user');
      if (jsonUser != null) {
        const user: IUser = JSON.parse(jsonUser) as IUser;
        if (user != null) {
          switch (user.roles[0].type) {
            default:
              throw new Error('Unknown type');
            case UserType.STUDENT:
              if (
                didacticActivity.courseName &&
                didacticActivity.color != DidacticActivityColor.RED
              ) {
                this.props.navigation.navigate('RequestView', {
                  didacticItem: didacticActivity,
                  onGoBack: () => this.refresh(),
                });
              }
              break;
            case UserType.TEACHER:
              this.props.navigation.navigate('ChangeColor', {
                didacticItem: didacticActivity,
                onGoBack: () => this.refresh(),
              });
              break;
            case UserType.ADMINISTRATOR:
              throw new Error('ADMIN is not implemented3');
          }
        }
      }
    }
  }

  _onRefresh() {
    this.setState({refreshing: true});
    this.props.onRefresh();
    this.setState({refreshing: false});
  }

  render() {
    if (!this.state.hours.length)
      for (let index = 8; index <= 21; index++) {
        this.state.hours.push(index.toString());
      }

    return (
      <View>
        <View style={styles.headerDays}>
          {this.state.days.map(item => {
            return <Text>{item}</Text>;
          })}
        </View>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }>
          <View style={styles.currentLine} />
          <View>
            <View style={styles.marginHours}>
              {this.state.hours.map(item => {
                return <Text style={styles.itemHours}>{item}</Text>;
              })}
            </View>
            <View style={styles.gridContent}>
              {this.state.days.map(() => {
                {
                  return (
                    <View>
                      {this.state.hours.map(() => {
                        return (
                          <View
                            style={[
                              styles.gridItem,
                              {
                                paddingRight:
                                  (WIDTH - 20) / this.state.days.length,
                              },
                            ]}></View>
                        );
                      })}
                    </View>
                  );
                }
              })}
            </View>

            <View style={styles.gridContent}>
              {this.state.days.map((_, collumn) => {
                {
                  return (
                    <View>
                      {this.state.hours.map((_, row) => {
                        const didactic: IDidacticActivity &
                          ITeacherActivity = this.getActivityByPosition(
                          collumn,
                          row,
                        );
                        return (
                          <TouchableHighlight
                            underlayColor={didactic.courseName ? '#3c4245' : ''}
                            onPress={() => this.openRequest(didactic)}>
                            <View
                              style={[
                                styles.didacticItem,
                                {
                                  width:
                                    (WIDTH - 20) / this.state.days.length + 1,
                                  height: 60,
                                  backgroundColor: this.getColorByType(
                                    didactic.color,
                                    didactic.type,
                                  ),
                                },
                              ]}>
                              {
                                <WeekViewItem
                                  didacticActivity={didactic}></WeekViewItem>
                              }
                            </View>
                          </TouchableHighlight>
                        );
                      })}
                    </View>
                  );
                }
              })}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerDays: {
    height: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
    borderColor: 'grey',
    borderWidth: 0.5,
    paddingLeft: 30,
    opacity: 0.7,
  },
  marginHours: {
    width: 20,
    borderWidth: 0.5,
    borderColor: 'grey',
    opacity: 0.7,
    borderTopWidth: 0,
  },
  itemHours: {
    paddingBottom: 40,
  },
  gridContent: {
    top: 0,
    flexDirection: 'row',
    position: 'absolute',
    left: 20,
  },
  gridItem: {
    borderWidth: 0.5,
    borderColor: 'rgba(10, 10, 10, 0.1)',
    paddingBottom: 59,
    opacity: 0.7,
  },
  didacticItem: {
    paddingBottom: 120,
    opacity: 0.7,
  },
  currentLine: {
    borderWidth: 1,
    top: (new Date().getHours() - 8) * 60 + new Date().getMinutes(),
    borderColor: 'green',
    zIndex: 1,
  },
});
