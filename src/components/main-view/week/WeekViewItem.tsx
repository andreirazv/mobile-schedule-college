import * as React from 'react';
import IDidacticActivity from '../../../data-models/DidacticActivity';
import {View, Text} from 'native-base';
import {StyleSheet} from 'react-native';
import {Fragment} from 'react';

interface IWeekViewItemProps {
  didacticActivity: IDidacticActivity;
}
export default class WeekViewItem extends React.Component<
  IWeekViewItemProps,
  any
> {
  constructor(props: IWeekViewItemProps) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        {this.props.didacticActivity.tutor != null && (
          <Fragment>
            <Text style={styles.fontText}>
              {this.props.didacticActivity.courseName.substring(0, 20) + '.'}
            </Text>
            <Text style={styles.fontText}>
              {this.props.didacticActivity.room}
            </Text>
            <Text style={styles.fontText}>
              {this.props.didacticActivity.tutor.lastName}
            </Text>
            <Text style={styles.fontText}>
              {this.props.didacticActivity.type}
            </Text>
          </Fragment>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  permissionItem: {
    position: 'absolute',
    alignSelf: 'flex-end',
  },
  container: {
    paddingLeft: 5,
    flexDirection: 'column',
  },
  fontText: {
    fontSize: 12,
  },
});
