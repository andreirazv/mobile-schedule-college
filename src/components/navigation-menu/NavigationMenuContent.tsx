import {Text, View} from 'native-base';
import React from 'react';
import {SafeAreaView, SectionList} from 'react-native';

import {INavMenuSection} from '../../data-models/NavMenuSection';
import NavigationMenuItem from './NavigationMenuItem';
import styles from './styles/NavMenuStyle';
import {IUser} from '../../data-models/User';
import AsyncStorage from '@react-native-community/async-storage';
import {UserType} from '../../data-models/UserType';

interface IPropsNavMenuContent {
  navigation: any;
}
interface IStateNavMenuContent {
  menuSections: any;
}
export default class NavigationMenuContent extends React.Component<
  IPropsNavMenuContent,
  IStateNavMenuContent
> {
  constructor(props: IPropsNavMenuContent) {
    super(props);
    this.state = {
      menuSections: [],
    };
  }

  private navigateToViewActivity = async () => {
    this.props.navigation.navigate('TabViewActivity');
  };

  private navigateToMainView() {
    this.props.navigation.navigate('MainView');
  }

  private navigateToAllRequest() {
    this.props.navigation.navigate('AllRequest');
  }

  private navigateToChangePassword() {
    this.props.navigation.navigate('ChangePassword');
  }

  private async isStudent(): Promise<boolean> {
    const jsonUser = await AsyncStorage.getItem('logged_user');
    if (jsonUser != null) {
      const user: IUser = JSON.parse(jsonUser) as IUser;
      if (user != null && user.id && user.roles[0].type === UserType.STUDENT) {
        return true;
      }
    }
    return false;
  }

  componentDidMount() {
    const menuSections: INavMenuSection[] = [
      {
        sectionText: 'General',
        data: [
          {
            sectionItemText: 'Dashboard',
            iconName: 'dashboard',
            iconClass: 'MaterialIcon',
            onClick: () => this.navigateToMainView(),
          },
        ],
      },
      {
        sectionText: 'Settings',
        data: [
          {
            sectionItemText: 'Change password',
            iconName: 'key-change',
            iconClass: 'MaterialCommunityIcons',
            onClick: () => this.navigateToChangePassword(),
          },
        ],
      },
    ];
    this.isStudent().then(isStudent => {
      if (isStudent) {
        const navItem1 = {
          sectionItemText: 'Select activities',
          iconName: 'reorder',
          iconClass: 'MaterialIcon',
          onClick: () => this.navigateToViewActivity(),
        };
        const navItem2 = {
          sectionItemText: 'All requests',
          iconName: 'update',
          iconClass: 'MaterialIcon',
          onClick: () => this.navigateToAllRequest(),
        };
        menuSections[0].data.push(navItem1);
        menuSections[0].data.push(navItem2);
        this.setState({menuSections: menuSections});
      } else {
        this.setState({menuSections: menuSections});
      }
    });
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <SectionList
          sections={this.state.menuSections}
          keyExtractor={(item, index) => item + index}
          renderItem={({item}) => <NavigationMenuItem menuItem={item} />}
          renderSectionHeader={({section: {sectionText}}) => (
            <View>
              <View style={[styles.lineStyle, styles.itemOpacity]} />
              {sectionText.length > 0 && (
                <Text style={[styles.sectionText, styles.textFormat]}>
                  {sectionText}
                </Text>
              )}
            </View>
          )}
        />
        <View style={[styles.lineStyle, styles.itemOpacity]} />
      </SafeAreaView>
    );
  }
}
