import React from 'react';
import {Content, List, Container, Left, Text, View} from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import {ScrollView} from 'react-native-gesture-handler';
import {TouchableNativeFeedback} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import NavigationMenuContent from './NavigationMenuContent';
import styles from './styles/NavMenuStyle';
import LanguageService from '../../services/LanguageService';
import {IUser} from '../../data-models/User';

interface IStateNavigationMenu {
  user: IUser;
  languageService: LanguageService;
}

interface IPropsNavMenu {
  navigation: any;
}

export default class NavigationMenu extends React.Component<
  IPropsNavMenu,
  IStateNavigationMenu
> {
  constructor(props: IPropsNavMenu) {
    super(props);
    this.state = {
      user: Object(null),
      languageService: new LanguageService(),
    };
  }

  async componentDidMount() {
    const jsonUser = await AsyncStorage.getItem('logged_user');
    if (jsonUser != null) {
      const user: IUser = JSON.parse(jsonUser) as IUser;
      if (user != null && user.id) {
        this.setState({
          user: user,
        });
      }
    }
  }

  private async clearStoredData() {
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('logged_user');
  }

  private logout() {
    this.clearStoredData();
    this.props.navigation.navigate('Login');
  }

  render() {
    return (
      <ScrollView style={styles.navDrawBg}>
        <Container style={styles.navDrawBg}>
          <View style={{alignItems: 'center'}}>
            <View style={styles.headerNavMenu}>
              <Text style={[styles.titleNavMenu, styles.textFormat]}>
                SCHED.
              </Text>
              {this.state.user.grupa && (
                <Text style={[styles.subTitleNavMenu, styles.textFormat]}>
                  {this.state.user.grupa}
                  {'/'}
                  {this.state.user.semigrupa}
                </Text>
              )}
            </View>
            <Text style={[styles.textFormat]}>
              {this.state.user.firstName} {this.state.user.lastName}
            </Text>
          </View>
          <Content>
            <List>
              <NavigationMenuContent navigation={this.props.navigation} />
            </List>
          </Content>
          <View style={[styles.lineStyle, styles.itemOpacity]} />
          <TouchableNativeFeedback
            onPress={this.logout.bind(this)}
            background={TouchableNativeFeedback.SelectableBackground()}>
            <View
              style={[
                styles.navMenuButton,
                styles.navDrawBg,
                {marginLeft: 10, marginBottom: 0},
              ]}>
              <Feather
                style={styles.itemOpacity}
                color={'white'}
                size={18}
                name={'log-out'}
              />
              <Left style={{marginLeft: 10}}>
                <Text style={[styles.navMenuButtonText, styles.textFormat]}>
                  {this.state.languageService.get('logout')}
                </Text>
              </Left>
            </View>
          </TouchableNativeFeedback>
        </Container>
      </ScrollView>
    );
  }
}
