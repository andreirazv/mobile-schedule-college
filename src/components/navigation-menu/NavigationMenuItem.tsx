import {View, Text, Left} from 'native-base';
import React from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {TouchableNativeFeedback} from 'react-native';

import {INavMenuSectionItem} from '../../data-models/NavMenuSectionItem';
import styles from './styles/NavMenuStyle';

interface IPropsNavMenuItem {
  menuItem: INavMenuSectionItem;
}

export default class NavigationMenuItem extends React.Component<
  IPropsNavMenuItem,
  any
> {
  constructor(props: IPropsNavMenuItem) {
    super(props);
  }

  render() {
    let iconView: any;
    const iconSize: number = 18;
    const iconColor: string = 'white';

    switch (this.props.menuItem.iconClass) {
      case 'MaterialCommunityIcons':
        iconView = (
          <MaterialCommunityIcons
            color={iconColor}
            style={styles.itemOpacity}
            size={iconSize}
            name={this.props.menuItem.iconName}
          />
        );
        break;
      default:
      case 'MaterialIcons':
        iconView = (
          <MaterialIcons
            style={styles.itemOpacity}
            color={iconColor}
            size={iconSize}
            name={this.props.menuItem.iconName}
          />
        );
        break;
    }

    return (
      <TouchableNativeFeedback
        onPress={this.props.menuItem.onClick}
        background={TouchableNativeFeedback.SelectableBackground()}>
        <View style={styles.navMenuButton}>
          {iconView}
          <Left style={{marginLeft: 10}}>
            <Text style={[styles.navMenuButtonText, styles.textFormat]}>
              {this.props.menuItem.sectionItemText}
            </Text>
          </Left>
        </View>
      </TouchableNativeFeedback>
    );
  }
}
