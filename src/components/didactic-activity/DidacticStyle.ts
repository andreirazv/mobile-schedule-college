import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  didacticItemView: {
    backgroundColor: '#c5d6d1',
    margin: 15,
    marginBottom: 0,
    borderColor: '#ffffff',
    flexDirection: 'column',
    justifyContent: 'space-between',
    borderRadius: 10,
  },
  itemPressed: {
    margin: 5,
    opacity: 0,
    backgroundColor: '#d6e2de',
  },
  textStyle: {
    fontSize: 18,
  },
  textCenterView: {
    alignItems: 'center',
    marginBottom: 10,
  },
  alignTextLine: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    flex: 1,
    marginTop: 5,
    marginBottom: 5,
  },
});

export default styles;
