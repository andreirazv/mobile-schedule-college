import React from 'react';
import {View, Text} from 'native-base';
import DidacticItem from './DidacticItem';
import {ScrollView, StyleSheet, Modal, RefreshControl} from 'react-native';
import IDidacticActivity from '../../data-models/DidacticActivity';
import DidacticActivityService from '../../services/DidacticActivityService';
import LanguageService from '../../services/LanguageService';
import RequestService from '../../services/RequestService';
import AsyncStorage from '@react-native-community/async-storage';
import {RequestStatus} from '../../data-models/RequestStatus';
import {RequestType} from '../../data-models/RequestType';
import {IUser} from '../../data-models/User';
import ScButton from '../sc-button/ScButton';
import ScTextInput from '../sc-text-input/ScTextInput';

interface IStateRequestView {
  didacticActivities: IDidacticActivity[];
  didacticItem: IDidacticActivity;
  newDidacticItem: IDidacticActivity;
  isModalVisible: boolean;
  refreshing: boolean;
  description: string;
  languageService: LanguageService;
}

export default class RequestView extends React.Component<
  any,
  IStateRequestView
> {
  constructor(props: any) {
    super(props);
    const {params} = this.props.navigation.state;
    this.state = {
      didacticActivities: [],
      didacticItem: params ? params.didacticItem : null,
      newDidacticItem: params ? params.didacticItem : null,
      isModalVisible: false,
      description: '',
      languageService: new LanguageService(),
      refreshing: false,
    };
  }

  private handleRequest() {
    DidacticActivityService.getActivitiesForChange(
      this.state.didacticItem,
    ).then((didacticList: IDidacticActivity[]) => {
      this.setState({
        didacticActivities: didacticList,
      });
    });
  }

  componentDidMount() {
    this.handleRequest();
  }

  private changeModalVisibility(visible: boolean) {
    this.setState({
      isModalVisible: visible,
    });
  }

  private onChangeDescription = (changedText: string) => {
    this.setState({
      description: changedText,
    });
  };

  async handleSend() {
    this.changeModalVisibility(!this.state.isModalVisible);

    const jsonUser = await AsyncStorage.getItem('logged_user');
    if (jsonUser != null) {
      const requester: IUser = JSON.parse(jsonUser) as IUser;
      const date = new Date();
      const format =
        date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
      RequestService.createRequest({
        type: RequestType.REQUEST,
        description: this.state.description,
        date: Date.parse(new Date().toString()),
        time: format,
        status: RequestStatus.PENDING,
        requester: requester,
        currentActivityId: this.state.newDidacticItem.id,
        activity: this.state.didacticItem,
      })
        .then(() => {
          this.props.navigation.state.params.onGoBack();
          this.props.navigation.goBack();
        })
        .catch(err => {
          alert(err);
        });
    }
  }

  private handleItemClick = (clickedItem: IDidacticActivity) => {
    this.setState({
      newDidacticItem: clickedItem,
    });
    this.changeModalVisibility(true);
  };

  _onRefresh() {
    this.setState({refreshing: true});
    this.handleRequest();
    this.setState({refreshing: false});
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <Modal
          animationType="fade"
          transparent={false}
          visible={this.state.isModalVisible}>
          <View>
            <View style={styles.header}>
              <ScButton
                onClick={() =>
                  this.changeModalVisibility(!this.state.isModalVisible)
                }
                text={this.state.languageService.get('back')}
                color={'#719192'}
                width={90}
              />
            </View>
            <View style={styles.modalContent}>
              <ScTextInput
                value={this.state.description}
                placeHolder={this.state.languageService.get('description')}
                onChangeText={this.onChangeDescription.bind(this)}
                width={20}
                icon={'note'}
              />
              <View style={styles.sendButton}>
                <ScButton
                  onClick={this.handleSend.bind(this)}
                  text={this.state.languageService.get('send')}
                />
              </View>
            </View>
          </View>
        </Modal>
        <View style={styles.titleView}>
          <Text style={styles.titleText}>
            {this.state.didacticItem.courseName}
            {'\n'}
            Select activity to change:
          </Text>
        </View>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }>
          {this.state.didacticActivities.map((item: IDidacticActivity) => {
            return (
              <DidacticItem
                activity={item}
                onClick={() => this.handleItemClick(item)}
              />
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  titleView: {
    alignItems: 'center',
    backgroundColor: '#719192',
  },
  titleText: {
    color: '#FFFFFF',
    fontSize: 20,
    textAlign: 'center',
    padding: 20,
  },
  modalContent: {
    marginTop: 100,
  },
  sendButton: {
    alignItems: 'center',
  },
  header: {
    height: 50,
    backgroundColor: '#719192',
  },
});
