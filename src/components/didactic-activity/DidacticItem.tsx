import React from 'react';
import {View, Text} from 'native-base';
import {TouchableHighlight} from 'react-native-gesture-handler';
import IDidacticActivity from '../../data-models/DidacticActivity';
import styles from './DidacticStyle';
import {WeekType} from '../../data-models/WeekType';
import LanguageService from '../../services/LanguageService';

interface IPropsDidacticItem {
  activity: IDidacticActivity;
  onClick(): void;
}

interface IStateDidacticItem {
  languageService: LanguageService;
  pressStatus: boolean;
}

export default class DidacticItem extends React.Component<
  IPropsDidacticItem,
  IStateDidacticItem
> {
  constructor(props: IPropsDidacticItem) {
    super(props);
    this.state = {
      pressStatus: false,
      languageService: new LanguageService(),
    };
  }

  private _onHideUnderlay() {
    this.setState({pressStatus: false});
  }

  private _onShowUnderlay() {
    this.setState({pressStatus: true});
  }

  private deleteSecoundFromTime(time: string): string {
    return time.slice(0, -3);
  }

  private getFullDate(date: number): string {
    var today = new Date(date);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    return dd + '-' + mm + '-' + yyyy;
  }

  private capitalize(item: string): string {
    return item.charAt(0).toUpperCase() + item.slice(1);
  }

  private getEndDate(time: string): string {
    const startTime = this.deleteSecoundFromTime(time);
    const newStartTime = Number.parseInt(startTime.slice(0, 2));
    return newStartTime + this.props.activity.duration + ':00';
  }

  private getWeekType(item: WeekType) {
    switch (item) {
      case WeekType.ODD:
        return 'W1';
      case WeekType.EVEN:
        return 'W2';
    }
  }

  render() {
    return (
      <TouchableHighlight
        underlayColor="none"
        onPress={() => this.props.onClick()}
        onHideUnderlay={this._onHideUnderlay.bind(this)}
        onShowUnderlay={this._onShowUnderlay.bind(this)}>
        <View
          style={
            this.state.pressStatus
              ? [styles.didacticItemView, styles.itemPressed]
              : styles.didacticItemView
          }>
          <View style={styles.alignTextLine}>
            <Text style={styles.textStyle}>
              {this.props.activity.semigroup && (
                <Text style={styles.textStyle}>
                  {this.state.languageService.get('group')}{' '}
                  {this.props.activity.group}
                  {'/'}
                  {this.props.activity.semigroup}
                </Text>
              )}
              {!this.props.activity.semigroup && (
                <Text style={styles.textStyle}>
                  {this.state.languageService.get('group')}{' '}
                  {this.props.activity.group}
                </Text>
              )}
            </Text>
            <Text style={styles.textStyle}>
              {this.props.activity.tutor.firstName}{' '}
              {this.props.activity.tutor.lastName}
            </Text>
          </View>

          <View style={styles.alignTextLine}>
            <Text style={styles.textStyle}>
              {this.capitalize(this.props.activity.dayOfTheWeek.toLowerCase())}
            </Text>
            <Text style={styles.textStyle}>
              {this.deleteSecoundFromTime(this.props.activity.time)}
              {' - '}
              {this.getEndDate(this.props.activity.time)}
            </Text>
            <Text style={styles.textStyle}>
              {this.getWeekType(this.props.activity.weekType)}
            </Text>
          </View>
          <View style={styles.textCenterView}>
            <Text style={styles.textStyle}>
              {this.props.activity.description}
            </Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}
