import React, {Fragment} from 'react';
import {View} from 'native-base';
import {Animated, Easing} from 'react-native';

import styles from './ActivityIndicatorStyle';

interface IStateActivity {
  animatedValueLong: Animated.Value;
  animatedValueShort: Animated.Value;
  interpolatedRotateAnimationLong: Animated.AnimatedInterpolation;
  interpolatedRotateAnimationShort: Animated.AnimatedInterpolation;
}

export default class ActivityIndicator extends React.Component<
  any,
  IStateActivity
> {
  constructor(props: any) {
    super(props);
    const animatedShort = new Animated.Value(0);
    const animatedLong = new Animated.Value(0);
    this.state = {
      animatedValueLong: animatedLong,
      animatedValueShort: animatedShort,
      interpolatedRotateAnimationLong: animatedLong.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg'],
      }),
      interpolatedRotateAnimationShort: animatedShort.interpolate({
        inputRange: [0, 1],
        outputRange: ['-90deg', '270deg'],
      }),
    };
  }

  componentDidMount() {
    this.runAnimation();
  }

  private runAnimation() {
    const time = 1000;
    Animated.loop(
      Animated.timing(this.state.animatedValueShort, {
        toValue: 1,
        duration: time * 6,
        easing: Easing.linear,
      }),
    ).start();
    Animated.loop(
      Animated.timing(this.state.animatedValueLong, {
        toValue: 1,
        easing: Easing.linear,
        duration: time,
      }),
    ).start();
  }

  render() {
    const animatedStyleShort = {
      transform: [
        {translateX: -13},
        {rotate: this.state.interpolatedRotateAnimationShort},
        {translateX: 13},
      ],
    };
    const animatedStyleLong = {
      transform: [
        {translateX: -18},
        {rotate: this.state.interpolatedRotateAnimationLong},
        {translateX: 18},
      ],
    };
    return (
      <Fragment>
        <View style={[styles.container, styles.background]} />
        <View style={[styles.alignElements, styles.container]}>
          <View style={[styles.clock, styles.alignElements]}>
            <View style={[styles.border1, styles.alignElements]}>
              <View style={[styles.border2, styles.alignElements]}>
                <Animated.View style={[styles.longSquare, animatedStyleLong]} />
                <Animated.View
                  style={[styles.shortSquare, animatedStyleShort]}
                />
                <View style={styles.centerPoint} />
              </View>
            </View>
          </View>
        </View>
      </Fragment>
    );
  }
}
