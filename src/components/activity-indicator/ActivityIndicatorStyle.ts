import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  background: {
    backgroundColor: 'black',
    opacity: 0.8,
  },
  container: {
    position: 'absolute',
    zIndex: 1000,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  clock: {
    height: 100,
    width: 100,
    backgroundColor: 'grey',
    borderRadius: 50,
  },
  border1: {
    height: 90,
    width: 90,
    backgroundColor: '#3c4245',
    borderRadius: 50,
  },
  border2: {
    height: 80,
    width: 80,
    backgroundColor: 'white',
    borderRadius: 50,
  },
  centerPoint: {
    height: 15,
    width: 15,
    backgroundColor: '#5f6769',
    borderRadius: 50,
  },
  longSquare: {
    position: 'absolute',
    height: 5,
    width: 40,
    backgroundColor: '#719192',
    borderRadius: 20,
    left: 37.5,
  },
  shortSquare: {
    position: 'absolute',
    height: 5,
    width: 30,
    backgroundColor: '#719192',
    borderRadius: 20,
    left: 37.5,
  },
  alignElements: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    zIndex: 2000,
  },
});
export default styles;
