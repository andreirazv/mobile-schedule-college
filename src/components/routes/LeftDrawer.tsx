import React from 'react';
import {Dimensions} from 'react-native';

import NavigationMenu from '../navigation-menu/NavigationMenu';
import MainView from '../main-view/MainView';
import AllRequest from '../all-request/AllRequest';
import TabViewActivity from '../add-activity/AddActivity';
import ChangePassword from '../change-password/ChangePassword';
import AsyncStorage from '@react-native-community/async-storage';
import AddActivityLeftDrawer from '../add-activity/AddActivityLeftDrawer';
import ChangeColor from '../ChangeColor/ChangeColor';
import RequestView from '../didactic-activity/RequestView';

const reactNavigation = require('react-navigation-drawer');
const WIDTH = Dimensions.get('window').width;
const token = AsyncStorage.getItem('token');

const leftDrawer = reactNavigation.createDrawerNavigator(
  {
    MainView: {screen: MainView},
    AllRequest: {screen: AllRequest},
    ChangePassword: {screen: ChangePassword},
    ChangeColor: {screen: ChangeColor},
    TabViewActivity: {
      screen: token != null ? AddActivityLeftDrawer : TabViewActivity,
    },
  },
  {
    initialRouteName: 'MainView',
    drawerWidth: WIDTH * 0.7,
    drawerPosition: 'left',
    drawerType: 'back',
    contentComponent: (props: any) => <NavigationMenu {...props} />,
    drawerOpenRoute: 'LeftSideMenu',
    drawerCloseRoute: 'LeftSideMenuClose',
    drawerToggleRoute: 'LeftSideMenuToggle',
  },
);

export default leftDrawer;
