import {createAppContainer} from 'react-navigation';

import LeftDrawer from './LeftDrawer';
import RegisterView from '../register/RegisterView';
import LoginView from '../login/LoginView';
import RequestView from '../didactic-activity/RequestView';
import TabViewActivity from '../add-activity/AddActivity';
import AllRequest from '../all-request/AllRequest';
import ChangePassword from '../change-password/ChangePassword';
import AddActivityLeftDrawer from '../add-activity/AddActivityLeftDrawer';
import AsyncStorage from '@react-native-community/async-storage';
import ChangeColor from '../ChangeColor/ChangeColor';

const reactNavigation = require('react-navigation-stack');
const token = AsyncStorage.getItem('token');
const MainNavigator = reactNavigation.createStackNavigator(
  {
    LeftDrawer: {screen: LeftDrawer},
    Login: {screen: LoginView},
    Register: {screen: RegisterView},
    RequestView: {screen: RequestView},
    ChangeColor: {screen: ChangeColor},
    TabViewActivity: {
      screen: token != null ? AddActivityLeftDrawer : TabViewActivity,
    },
    AllRequest: {screen: AllRequest},
    ChangePassword: {screen: ChangePassword},
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
    swipeEnabled: false,
  },
);

const MainRoute = createAppContainer(MainNavigator);
export default MainRoute;
