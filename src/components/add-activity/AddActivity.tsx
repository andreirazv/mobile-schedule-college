import React from 'react';
import {Dimensions} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';

import CheckBoxList from './CheckBoxList';
import DidacticActivityService from '../../services/DidacticActivityService';
import IDidacticActivity from '../../data-models/DidacticActivity';
import AsyncStorage from '@react-native-community/async-storage';
import {IUser} from '../../data-models/User';

interface IStateTabView {
  index: number;
  routes: any[];
  renderScene: any;
  an1Courses: IDidacticActivity[];
  an2Courses: IDidacticActivity[];
  an3Courses: IDidacticActivity[];
  userId: number;
  enroledAn1: number[];
  enroledAn2: number[];
  enroledAn3: number[];
  goToLogin: boolean;
  isDisabledDrawer: boolean;
}

interface IPropsTabView {
  navigation: any;
}

export default class TabViewActivity extends React.Component<
  IPropsTabView,
  IStateTabView
> {
  constructor(props: IPropsTabView) {
    super(props);
    const {navigation} = this.props;

    this.state = {
      index: 0,
      an1Courses: [],
      an2Courses: [],
      an3Courses: [],
      enroledAn1: [],
      enroledAn2: [],
      enroledAn3: [],
      routes: [
        {key: 'an1', title: 'Anul 1'},
        {key: 'an2', title: 'Anul 2'},
        {key: 'an3', title: 'Anul 3'},
      ],
      renderScene: null,
      userId: navigation.getParam('userId', -1),
      goToLogin: navigation.getParam('goToLogin', false),
      isDisabledDrawer: navigation.getParam('userId', -1) != -1,
    };
  }

  checkBoxListRoute(items: IDidacticActivity[], enroled: number[]): any {
    return (
      <CheckBoxList
        goToLogin={this.state.goToLogin}
        navigation={this.props.navigation}
        enroled={enroled}
        userId={this.state.userId}
        isDisabledDrawer={this.state.isDisabledDrawer}
        list={items}
      />
    );
  }

  async componentDidMount() {
    if (this.state.userId == -1) {
      const jsonUser = await AsyncStorage.getItem('logged_user');
      if (jsonUser != null) {
        const user: IUser = JSON.parse(jsonUser) as IUser;
        if (user != null && user.id) {
          this.setState({
            userId: user.id,
          });
          DidacticActivityService.getActivities(user).then((courses: any[]) => {
            this.setState({
              enroledAn3: courses.map(item => item.id),
            });
          });
        }
      }
    }
    DidacticActivityService.getAllCourses().then(
      (courses: IDidacticActivity[]) => {
        this.setState({
          an3Courses: courses,
        });
      },
    );
  }

  render() {
    return (
      <TabView
        renderTabBar={renderTabBar}
        tabBarPosition={'bottom'}
        navigationState={this.state}
        renderScene={SceneMap({
          an1: () =>
            this.checkBoxListRoute(
              this.state.an1Courses,
              this.state.enroledAn1,
            ),
          an2: () =>
            this.checkBoxListRoute(
              this.state.an2Courses,
              this.state.enroledAn2,
            ),
          an3: () =>
            this.checkBoxListRoute(
              this.state.an3Courses,
              this.state.enroledAn3,
            ),
        })}
        onIndexChange={index => this.setState({index: index})}
        initialLayout={{width: Dimensions.get('window').width}}
      />
    );
  }
}

const renderTabBar = (props: any) => (
  <TabBar
    {...props}
    indicatorStyle={{backgroundColor: 'white'}}
    style={{backgroundColor: '#719192'}}
  />
);
