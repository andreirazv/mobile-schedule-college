import React from 'react';

import TabViewActivity from './AddActivity';

interface IPropsAddActivityLeft {
  navigation: any;
}

export default class AddActivityLeftDrawer extends React.Component<
  IPropsAddActivityLeft,
  any
> {
  constructor(props: any) {
    super(props);
  }

  render() {
    return <TabViewActivity navigation={this.props.navigation} />;
  }
}
