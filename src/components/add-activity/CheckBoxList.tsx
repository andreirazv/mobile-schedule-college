import React from 'react';
import {ScrollView} from 'react-native';
import {CheckBox} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {AxiosError} from 'axios';
import {Container, View, Text} from 'native-base';

import CustomHeader from '../navigation-menu/custom-header/CustomHeader';
import styles from '../navigation-menu/styles/NavMenuStyle';
import IDidacticActivity from '../../data-models/DidacticActivity';
import StudentDidacticService from '../../services/StudentDidacticService';
import LanguageService from '../../services/LanguageService';
import Snackbar from 'react-native-snackbar';

interface IPropsCheckBox {
  list: IDidacticActivity[];
  goToLogin: boolean;
  navigation: any;
  userId: number;
  enroled: number[];
  isDisabledDrawer: boolean;
}

interface IStateCheckBox {
  selectedItems: number[];
  languageService: LanguageService;
}

export default class CheckBoxList extends React.Component<
  IPropsCheckBox,
  IStateCheckBox
> {
  constructor(props: IPropsCheckBox) {
    super(props);
    this.state = {
      selectedItems: this.props.enroled,
      languageService: new LanguageService(),
    };
  }

  private showSnackBarMessage(message: string) {
    Snackbar.show({
      title: message,
      duration: Snackbar.LENGTH_SHORT,
    });
  }

  sendAddRequest(id: number) {
    StudentDidacticService.createActivity({
      activityId: id,
      userId: this.props.userId,
    })
      .then(x => {
        this.showSnackBarMessage('Activity was added');
      })
      .catch((error: AxiosError) => {
        alert(this.state.languageService.get('general_error'));
      });
  }
  sendDeleteRequest(id: number) {
    StudentDidacticService.deleteActivity({
      activityId: id,
      userId: this.props.userId,
    })
      .then(x => {
        this.showSnackBarMessage('Activity was deleted');
      })
      .catch((error: AxiosError) => {
        alert(this.state.languageService.get('general_error'));
      });
  }

  addActivity(id: number) {
    let tmp = this.state.selectedItems;
    if (tmp.includes(id)) {
      var index = tmp.indexOf(id);
      if (index !== -1) {
        tmp.splice(index, 1);
        this.sendDeleteRequest(id);
      }
    } else {
      tmp.push(id);
      this.sendAddRequest(id);
    }
    this.setState({
      selectedItems: tmp,
    });
  }

  render() {
    return (
      <Container>
        <CustomHeader
          navigation={this.props.navigation}
          isDisabledDrawer={this.props.isDisabledDrawer}
          titleText="home">
          <View style={[styles.customHeader]}>
            <Text style={[styles.textFormat, styles.subTitleNavMenu]}>
              {this.state.languageService.get('choose_courses')}
            </Text>
          </View>
        </CustomHeader>
        <ScrollView>
          {this.props.list.map((item, _) => {
            return (
              <CheckBox
                title={item.courseName}
                checked={
                  this.state.selectedItems.includes(item.id) ? true : false
                }
                onPress={() => {
                  this.addActivity(item.id);
                }}
                checkedColor={'#3c4245'}
              />
            );
          })}
        </ScrollView>
      </Container>
    );
  }
}
