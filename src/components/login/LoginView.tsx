import React, {Fragment} from 'react';
import {
  Image,
  View,
  StyleSheet,
  Dimensions,
  Text,
  KeyboardAvoidingView,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import * as jwt from 'jsontokens';
import Snackbar from 'react-native-snackbar';
import {sha256} from 'react-native-sha256';

import ScButton from '../sc-button/ScButton';
import ScPicker from '../sc-picker/ScPicker';
import ScTextInput from '../sc-text-input/ScTextInput';
import AuthenticateService from '../../services/AuthenticateService';
import {IUser} from '../../data-models/User';
import ActivityIndicator from '../activity-indicator/ActivityIndicator';
import LanguageService from '../../services/LanguageService';
import {Language} from '../../data-models/Language';

interface IStateLogin {
  username: string;
  password: string;
  isLoading: boolean;
  languageService: LanguageService;
  currentLanguage: Language;
}

interface Payload {
  sub: string;
  exp: number;
  iat: number;
}

export default class LoginView extends React.Component<any, IStateLogin> {
  constructor(props: any) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isLoading: false,
      languageService: new LanguageService(),
      currentLanguage: Object(null),
    };
    this.state.languageService.getLanguage().then((lang: Language) => {
      this.setState({
        currentLanguage: lang,
      });
    });
    //this.props.navigation.navigate('MainView');
    this.checkLogin();
  }

  private async checkLogin() {
    const token = (await AsyncStorage.getItem('token')) || null;
    if (!token) return;

    const decodedToken: Payload = JSON.parse(
      JSON.stringify(jwt.decodeToken(token).payload),
    );
    const dateNow = new Date();
    const tokenDate = new Date(0);
    tokenDate.setUTCSeconds(decodedToken.exp);

    tokenDate.getTime() < dateNow.getTime()
      ? this.clearStoredData()
      : this.props.navigation.navigate('MainView');
  }

  private onPressLoginButton() {
    //  sha256(this.state.password).then((hash: string) => {
    // this.handleLogin(this.state.username, hash);
    this.handleLogin(this.state.username, this.state.password);
    // });
  }

  private showSnackBarMessage(message: string) {
    Snackbar.show({
      title: message,
      duration: Snackbar.LENGTH_LONG,
    });
  }

  private handleLogin(username: string, password: string) {
    this.setState({
      isLoading: true,
    });

    AuthenticateService.login({
      username,
      password,
    })
      .then(async (user: IUser) => {
        await AsyncStorage.setItem('logged_user', JSON.stringify(user));
        await AsyncStorage.setItem('token', user.token);
        this.setState({
          isLoading: false,
          password: '',
          username: '',
        });
        this.props.navigation.navigate('MainView');
      })
      .catch(async () => {
        this.showSnackBarMessage(this.state.languageService.get('wrong_login'));
        this.clearStoredData();

        this.setState({
          isLoading: false,
        });
      });
  }

  private async clearStoredData() {
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('logged_user');
  }

  private switchToRegister() {
    this.props.navigation.navigate('Register');
  }

  private onChangeUserName = (changedText: string) => {
    this.setState({
      username: changedText,
    });
  };

  private onChangePassword = (changedText: string) => {
    this.setState({
      password: changedText,
    });
  };

  private onChangeLanguage = (changedLanguage: Language) => {
    this.state.languageService.setLanguage(changedLanguage);
    this.setState({
      currentLanguage: changedLanguage,
    });
  };

  render() {
    return (
      <Fragment>
        {this.state.isLoading === true && <ActivityIndicator />}
        <KeyboardAvoidingView behavior={'position'}>
          <View style={styles.loginView}>
            <ScPicker
              selectedValue={this.state.currentLanguage}
              onChangeValue={(itemValue: Language) =>
                this.onChangeLanguage(itemValue)
              }
              pickerItems={Object.keys(Language)}
            />

            <View style={styles.profileBackground}>
              <View style={styles.photoStyle}>
                <Image source={require('../../assets/person-icon.png')} />
              </View>
            </View>
            <View style={styles.formView}>
              <ScTextInput
                value={this.state.username}
                placeHolder={this.state.languageService.get('usrname_email')}
                icon={'mail'}
                onChangeText={this.onChangeUserName.bind(this)}
              />
              <ScTextInput
                value={this.state.password}
                placeHolder={this.state.languageService.get('password')}
                icon={'lock'}
                secureTextEntry={true}
                onChangeText={this.onChangePassword.bind(this)}
              />
            </View>
            <View style={styles.textInput}>
              <ScButton
                text={this.state.languageService.get('login')}
                onClick={this.onPressLoginButton.bind(this)}
              />
              <Text
                style={styles.textStyle}
                onPress={this.switchToRegister.bind(this)}>
                {this.state.languageService.get('create_accont')}
              </Text>
            </View>
          </View>
        </KeyboardAvoidingView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  loginView: {
    alignItems: 'center',
  },
  textInput: {
    padding: 10,
    alignItems: 'center',
  },
  formView: {
    paddingTop: 20,
    width: Dimensions.get('screen').width - 60,
  },
  profileBackground: {
    width: '100%',
    height: '53%',
    backgroundColor: '#719192',
  },
  photoStyle: {
    alignItems: 'center',
  },
  textStyle: {
    paddingTop: 10,
    fontSize: 15,
    fontWeight: 'bold',
    color: '#719192',
    textDecorationLine: 'underline',
  },
});
