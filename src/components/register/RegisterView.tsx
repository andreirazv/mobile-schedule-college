import React, {Fragment} from 'react';
import {Text, View, KeyboardAvoidingView} from 'react-native';
import {sha256} from 'react-native-sha256';
import {AxiosError} from 'axios';
import Snackbar from 'react-native-snackbar';

import ScButton from '../sc-button/ScButton';
import ScTextInput from '../sc-text-input/ScTextInput';
import ScPicker from '../sc-picker/ScPicker';
import ServGrupe from '../../services/RegisterService';
import {CheckBox} from 'react-native-elements';
import {IUser} from '../../data-models/User';
import UserService from '../../services/UserService';
import ActivityIndicator from '../activity-indicator/ActivityIndicator';
import LanguageService from '../../services/LanguageService';
import {UserType} from '../../data-models/UserType';
import {ScrollView} from 'react-native-gesture-handler';
import styles from './RegisterStyles';
import IRole from '../../data-models/Role';

interface IScRegisterState {
  firstname: string;
  lastname: string;
  email: string;
  password: string;
  secondPassword: string;
  group: string;
  semigroup: string;
  mobileNumber: string;
  isLoading: boolean;
  checked: boolean;
  isValidLastName: boolean;
  isValidFirstName: boolean;
  isValidEmail: boolean;
  isValidPassword: boolean;
  isValidMobileNumber: boolean;
  isValidAgreeTerm: boolean;
  languageService: LanguageService;
  groups: number[];
}

export default class RegisterView extends React.Component<
  any,
  IScRegisterState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      firstname: '',
      lastname: '',
      email: '',
      password: '',
      secondPassword: '',
      group: '',
      semigroup: '',
      mobileNumber: '',
      checked: false,
      isLoading: false,
      isValidLastName: true,
      isValidFirstName: true,
      isValidEmail: true,
      isValidPassword: true,
      isValidAgreeTerm: true,
      isValidMobileNumber: true,
      groups: [],
      languageService: new LanguageService(),
    };
  }

  private showSnackBarMessage(message: string) {
    Snackbar.show({
      title: message,
      duration: Snackbar.LENGTH_LONG,
    });
  }

  private checkInputData(): boolean {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let isNotError = true;
    if (this.state.lastname.length == 0) {
      this.setState({
        isValidLastName: false,
      });
      isNotError = false;
    }

    if (this.state.firstname.length == 0) {
      this.setState({
        isValidFirstName: false,
      });
      isNotError = false;
    }

    if (
      this.state.secondPassword.length == 0 ||
      this.state.password.length == 0
    ) {
      this.setState({
        isValidPassword: false,
      });
      isNotError = false;
    }
    if (!isNotError) {
      this.showSnackBarMessage(
        this.state.languageService.get('incomplete_data'),
      );
      return isNotError;
    }
    if (this.state.mobileNumber.length != 10) {
      this.showSnackBarMessage(this.state.languageService.get('invalid_phone'));
      this.setState({
        isValidMobileNumber: false,
      });
      return false;
    }

    if (reg.test(this.state.email) === false) {
      this.showSnackBarMessage(this.state.languageService.get('invalid_email'));
      this.setState({
        isValidEmail: false,
      });
      return false;
    }

    if (this.state.password !== this.state.secondPassword) {
      this.showSnackBarMessage(
        this.state.languageService.get('invalid_password'),
      );
      this.setState({
        isValidPassword: false,
      });
      return false;
    }

    if (!this.state.checked) {
      this.showSnackBarMessage(
        this.state.languageService.get('invalid_agree_terms'),
      );
      this.setState({
        isValidAgreeTerm: false,
      });
      return false;
    }

    return true;
  }

  componentDidMount() {
    ServGrupe.getGroups().then((items: number[]) => {
      this.setState({
        groups: items,
      });
    });
  }

  private onRegister() {
    if (!this.checkInputData()) return;

    this.setState({
      isLoading: true,
    });
    const roles: IRole[] = [
      {type: UserType.STUDENT, users: null, permissions: null},
    ];
    const user: IUser = {
      username: this.state.email.split('@')[0],
      firstName: this.state.firstname,
      lastName: this.state.lastname,
      email: this.state.email,
      password: '',
      mobileNumber: this.state.mobileNumber,
      status: 1,
      counter: 0,
      grupa: Number.parseInt(this.state.group),
      semigrupa: Number.parseInt(this.state.semigroup),
      token: '',
      roles: roles,
    };
    // sha256(this.state.password).then((hash: string) => {
    user.password = this.state.password;
    // user.password = hash;
    // });
    UserService.insert(user)
      .then((newUser: IUser) => {
        this.showSnackBarMessage(
          this.state.languageService.get('succes_create_account'),
        );
        this.setState({
          isLoading: false,
        });
        this.props.navigation.navigate('TabViewActivity', {
          goToLogin: true,
          userId: newUser.id,
        });
      })
      .catch((error: AxiosError) => {
        this.showSnackBarMessage(error.message);
        this.setState({
          isLoading: false,
        });
      });
  }

  render() {
    return (
      <ScrollView>
        <KeyboardAvoidingView>
          <Fragment>
            {this.state.isLoading === true && <ActivityIndicator />}
            <View style={styles.sgRegisterView}>
              <Text style={styles.sgTitleText}>
                {this.state.languageService.get('register_form')}
              </Text>
              <View style={styles.sgFormView}>
                <ScTextInput
                  value={this.state.lastname}
                  placeHolder={this.state.languageService.get('l_name')}
                  icon={'person'}
                  bgColor={!this.state.isValidLastName ? '#FAAABA' : ''}
                  onChangeText={newLastName =>
                    this.setState({
                      lastname: newLastName,
                      isValidLastName: true,
                    })
                  }
                />
                <ScTextInput
                  value={this.state.firstname}
                  placeHolder={this.state.languageService.get('f_name')}
                  icon={'person'}
                  bgColor={!this.state.isValidFirstName ? '#FAAABA' : ''}
                  onChangeText={newFirstName =>
                    this.setState({
                      firstname: newFirstName,
                      isValidFirstName: true,
                    })
                  }
                />
                <ScTextInput
                  value={this.state.mobileNumber}
                  placeHolder={this.state.languageService.get('phone')}
                  icon={'phone'}
                  keyboardType={'phone-pad'}
                  bgColor={!this.state.isValidMobileNumber ? '#FAAABA' : ''}
                  onChangeText={newMobileNumber =>
                    this.setState({
                      mobileNumber: newMobileNumber,
                      isValidMobileNumber: true,
                    })
                  }
                />
                <ScTextInput
                  value={this.state.email}
                  placeHolder={this.state.languageService.get('email')}
                  icon={'mail'}
                  keyboardType={'email-address'}
                  bgColor={!this.state.isValidEmail ? '#FAAABA' : ''}
                  onChangeText={newEmail =>
                    this.setState({email: newEmail, isValidEmail: true})
                  }
                />
                <ScTextInput
                  value={this.state.password}
                  placeHolder={this.state.languageService.get('password')}
                  icon={'lock'}
                  secureTextEntry={true}
                  bgColor={!this.state.isValidPassword ? '#FAAABA' : ''}
                  onChangeText={newPassword =>
                    this.setState({
                      password: newPassword,
                      isValidPassword: true,
                    })
                  }
                />
                <ScTextInput
                  value={this.state.secondPassword}
                  placeHolder={this.state.languageService.get('conf_pass')}
                  icon={'lock'}
                  secureTextEntry={true}
                  bgColor={!this.state.isValidPassword ? '#FAAABA' : ''}
                  onChangeText={newSecondPassowrd =>
                    this.setState({
                      secondPassword: newSecondPassowrd,
                      isValidPassword: true,
                    })
                  }
                />
                <View style={styles.sgPicker}>
                  <View>
                    <Text style={styles.sgText}>
                      {this.state.languageService.get('group')}
                    </Text>
                    <ScPicker
                      selectedValue={this.state.group}
                      pickerItems={this.state.groups.map(item =>
                        item.toString(),
                      )}
                      onChangeValue={newGroup =>
                        this.setState({group: newGroup})
                      }
                    />
                  </View>
                  <View>
                    <Text style={styles.sgText}>
                      {this.state.languageService.get('semigroup')}
                    </Text>
                    <ScPicker
                      selectedValue={this.state.semigroup}
                      pickerItems={ServGrupe.getSemigroups()}
                      onChangeValue={newSemiGroup =>
                        this.setState({semigroup: newSemiGroup})
                      }
                    />
                  </View>
                </View>
                <CheckBox
                  containerStyle={{
                    backgroundColor: !this.state.isValidAgreeTerm
                      ? '#FAAABA'
                      : 'white',
                  }}
                  title={this.state.languageService.get('agree_terms')}
                  checked={this.state.checked}
                  onPress={() =>
                    this.setState({
                      checked: !this.state.checked,
                      isValidAgreeTerm: true,
                    })
                  }
                  checkedColor={'#3c4245'}
                />
              </View>
              <ScButton
                onClick={this.onRegister.bind(this)}
                text={this.state.languageService.get('register')}
              />
            </View>
          </Fragment>
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }
}
