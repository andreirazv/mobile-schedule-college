import React from 'react';
import {View, Text} from 'native-base';
import IRequest from '../../data-models/Request';
import {TouchableHighlight} from 'react-native-gesture-handler';
import styles from '../didactic-activity/DidacticStyle';
import {WeekType} from '../../data-models/WeekType';
import LanguageService from '../../services/LanguageService';

interface IPropsViewRequestItem {
  request: IRequest;
  onClick(): void;
}

interface IStateViewRequestItem {
  pressStatus: boolean;
  languageService: LanguageService;
}

export default class ViewRequestItem extends React.Component<
  IPropsViewRequestItem,
  IStateViewRequestItem
> {
  constructor(props: IPropsViewRequestItem) {
    super(props);
    this.state = {
      pressStatus: false,
      languageService: new LanguageService(),
    };
  }

  private deleteSecoundFromTime(time: string): string {
    return time.slice(0, -3);
  }

  private getEndDate(time: string): string {
    const startTime = this.deleteSecoundFromTime(time);
    const newStartTime = Number.parseInt(startTime.slice(0, 2));
    return newStartTime + this.props.request.activity.duration + ':00';
  }

  private _onHideUnderlay() {
    this.setState({pressStatus: false});
  }

  private _onShowUnderlay() {
    this.setState({pressStatus: true});
  }

  private capitalize(item: string): string {
    return item.charAt(0).toUpperCase() + item.slice(1);
  }

  private getWeekType(item: WeekType) {
    switch (item) {
      case WeekType.ODD:
        return 'W1';
      case WeekType.EVEN:
        return 'W2';
    }
  }
  render() {
    return (
      <TouchableHighlight
        underlayColor="none"
        onPress={() => this.props.onClick()}
        onHideUnderlay={this._onHideUnderlay.bind(this)}
        onShowUnderlay={this._onShowUnderlay.bind(this)}>
        <View
          style={
            this.state.pressStatus
              ? [styles.didacticItemView, styles.itemPressed]
              : styles.didacticItemView
          }>
          <View style={styles.alignTextLine}>
            <Text style={styles.textStyle}>
              {this.props.request.activity.courseName.substring(0, 20) + '.'}
            </Text>
            <Text style={styles.textStyle}>
              {this.capitalize(this.props.request.activity.type.toLowerCase())}
            </Text>
          </View>
          <View style={styles.alignTextLine}>
            <Text style={styles.textStyle}>
              {this.props.request.activity.semigroup && (
                <Text style={styles.textStyle}>
                  {this.state.languageService.get('group')}{' '}
                  {this.props.request.activity.group}
                  {'/'}
                  {this.props.request.activity.semigroup}
                </Text>
              )}
              {!this.props.request.activity.semigroup && (
                <Text style={styles.textStyle}>
                  {this.state.languageService.get('group')}{' '}
                  {this.props.request.activity.group}
                </Text>
              )}
            </Text>
            <Text style={styles.textStyle}>
              {this.props.request.activity.tutor.firstName}{' '}
              {this.props.request.activity.tutor.lastName}
            </Text>
          </View>

          <View style={styles.alignTextLine}>
            <Text style={styles.textStyle}>
              {this.capitalize(
                this.props.request.activity.dayOfTheWeek.toLowerCase(),
              )}
            </Text>
            <Text style={styles.textStyle}>
              {this.deleteSecoundFromTime(this.props.request.activity.time)}
              {' - '}
              {this.getEndDate(this.props.request.activity.time)}
            </Text>
            <Text style={styles.textStyle}>
              {this.getWeekType(this.props.request.activity.weekType)}
            </Text>
          </View>
          <View style={styles.alignTextLine}>
            <Text style={styles.textStyle}>
              {this.props.request.requester.firstName}{' '}
              {this.props.request.requester.lastName}
            </Text>
            <Text style={styles.textStyle}>
              {this.props.request.description}
            </Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}
