import React, {Fragment} from 'react';
import {View, Container, Text} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';

import RequestService from '../../services/RequestService';
import {IUser} from '../../data-models/User';
import LanguageService from '../../services/LanguageService';
import CustomHeader from '../navigation-menu/custom-header/CustomHeader';
import styles from '../navigation-menu/styles/NavMenuStyle';
import ViewRequestItem from './ViewRequestItem';
import IRequest from '../../data-models/Request';
import ActivityIndicator from '../activity-indicator/ActivityIndicator';
import {ScrollView, RefreshControl} from 'react-native';
import {RequestType} from '../../data-models/RequestType';
import Snackbar from 'react-native-snackbar';

interface IStateAllRequest {
  isLoading: boolean;
  requests: IRequest[];
  languageService: LanguageService;
  user: IUser;
}

export default class AllRequest extends React.Component<any, IStateAllRequest> {
  constructor(props: any) {
    super(props);
    this.state = {
      isLoading: false,
      user: Object(null),
      requests: [],
      languageService: new LanguageService(),
    };
  }

  componentDidMount() {
    this.setUser();
    this.handleRequest();
  }

  private async setUser() {
    const jsonUser = await AsyncStorage.getItem('logged_user');
    if (jsonUser != null) {
      const user: IUser = JSON.parse(jsonUser) as IUser;
      if (user != null && user.id) {
        this.setState({
          user: user,
        });
      }
    }
  }

  private async handleRequest() {
    const jsonUser = await AsyncStorage.getItem('logged_user');
    if (jsonUser != null) {
      const user: IUser = JSON.parse(jsonUser) as IUser;
      if (user != null) {
        if (user.id != null) {
          this.setState({
            isLoading: true,
          });
          RequestService.getRequests(user.id)
            .then((item: IRequest[]) => {
              this.setState({
                isLoading: false,
                requests: item,
              });
            })
            .catch(() => {
              alert(this.state.languageService.get('general_error'));
              this.setState({
                isLoading: false,
              });
            });
        }
      }
    }
  }

  _onRefresh() {
    this.setState({isLoading: true});
    this.handleRequest();
    this.setState({isLoading: false});
  }

  private onAcceptRequest(item: IRequest) {
    item.type = RequestType.ACCEPT;
    item.requester = this.state.user;
    RequestService.createRequest(item)
      .then(() => {
        this._onRefresh();
        this.showSnackBarMessage('Activity was accepted!');
      })
      .catch(err => {
        alert(err);
      });
  }
  private showSnackBarMessage(message: string) {
    Snackbar.show({
      title: message,
      duration: Snackbar.LENGTH_LONG,
    });
  }

  render() {
    return (
      <Container>
        {this.state.isLoading && <ActivityIndicator />}
        <CustomHeader navigation={this.props.navigation} titleText="home">
          <View style={[styles.customHeader]}>
            <Text style={[styles.textFormat, styles.subTitleNavMenu]}>
              Select activity to change:
            </Text>
          </View>
        </CustomHeader>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.isLoading}
              onRefresh={this._onRefresh.bind(this)}
            />
          }>
          {this.state.requests.length == 0 && (
            <Text
              style={[
                styles.textFormat,
                styles.subTitleNavMenu,
                {color: 'black', textAlign: 'center'},
              ]}>
              There are no request right now!
            </Text>
          )}
          {this.state.requests.map(item => {
            return (
              <ViewRequestItem
                onClick={() => this.onAcceptRequest(item)}
                request={item}
              />
            );
          })}
        </ScrollView>
      </Container>
    );
  }
}
