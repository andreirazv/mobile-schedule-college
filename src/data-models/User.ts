import {UserType} from './UserType';
import IRole from './Role';

export interface IUser {
  id?: number;
  counter: number;
  firstName: string;
  lastName: string;
  mobileNumber: string;
  email: string;
  username: string;
  password: string;
  status: number;
  token: string;
  grupa: number;
  semigrupa: number;
  roles: IRole[];
}
