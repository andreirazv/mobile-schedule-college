import {IUser} from './User';
import {DidacticActivityType} from './DidacticActivityType';
import {DidacticActivityStatus} from './DidacticActivityStatus';
import {DayOfTheWeek} from './DayOfTheWeek';
import {DidacticActivityColor} from './DidacticActivityColor';
import {WeekType} from './WeekType';

export default interface IDidacticActivity {
  id: number;
  courseName: string;
  type: DidacticActivityType;
  date: number;
  time: string;
  duration: number;
  room: string;
  specialization: string;
  group: number;
  semigroup: number;
  status: DidacticActivityStatus;
  description: string;
  tutor: IUser;
  dayOfTheWeek: DayOfTheWeek;
  color: DidacticActivityColor;
  weekType: WeekType;
}
