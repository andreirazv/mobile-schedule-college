export enum RequestType {
  REQUEST = 'REQUEST',
  ACCEPT = 'ACCEPT',
  DENY = 'DENY',
}
