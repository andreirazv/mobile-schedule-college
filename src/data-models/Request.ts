import {IUser} from './User';
import IDidacticActivity from './DidacticActivity';
import {RequestStatus} from './RequestStatus';
import {RequestType} from './RequestType';

export default interface IRequest {
  id?: number;
  type: RequestType;
  description: string;
  date: number;
  time: string;
  status: RequestStatus;
  requester: IUser;
  activity: IDidacticActivity;
  currentActivityId: number;
}
