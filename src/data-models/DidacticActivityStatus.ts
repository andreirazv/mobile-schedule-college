export enum DidacticActivityStatus {
  NO_CHANGES = 'NO_CHANGES',
  ALL_CHANGES = 'ALL_CHANGES',
  SOME_CHANGES = 'SOME_CHANGES',
  CLOSED = 'CLOSED',
}
