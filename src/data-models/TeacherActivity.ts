export default interface ITeacherActivity {
  courseName: string;
  activityType: string;
  date: number;
  time: string;
  duration: number;
  room: string;
  specialization: string;
  group: number;
  semigroup: number;
  description: string;
}
