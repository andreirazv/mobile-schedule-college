import {IUser} from './User';
import IDidacticActivity from './DidacticActivity';

export interface IStudentDidacticManager {
  ID: string;
  studentDTO: IUser;
  activityDTO: IDidacticActivity;
}
