export interface IStudentDidacticRequest {
  userId: number;
  activityId: number;
}
