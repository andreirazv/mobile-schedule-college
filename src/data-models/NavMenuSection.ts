import {INavMenuSectionItem} from './NavMenuSectionItem';

export interface INavMenuSection {
  sectionText: string;
  data: INavMenuSectionItem[];
}
