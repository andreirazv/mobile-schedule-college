export interface INavMenuSectionItem {
  sectionItemText: string;
  iconName: string;
  iconClass: string;
  onClick(): void;
}
