export enum DidacticActivityType {
  COURSE = 'COURSE',
  SEMINARY = 'SEMINARY',
  LABORATORY = 'LABORATORY',
}
