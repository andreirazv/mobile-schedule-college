import axios from 'axios';

import {config} from '../config/config';
import {IUser} from '../data-models/User';
import IJwtRequest from '../data-models/JwtRequest';

export default class AuthenticateService {
  public static login = async (jwtRequest: IJwtRequest): Promise<IUser> => {
    const result = await axios.post(
      `${config.apiUrl}/authenticate`,
      jwtRequest,
    );
    return result.data;
  };
}
