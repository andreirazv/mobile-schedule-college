import axios from 'axios';
import {config} from '../config/config';

export default class RegisterService {
  public static getGroups = async (): Promise<number[]> => {
    const result = await axios.get(`${config.apiUrl}/users/groups`);
    return result.data;
  };

  public static getSemigroups(): string[] {
    return ['1', '2'];
  }
}
