import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

import {config} from '../config/config';
import IDidacticActivity from '../data-models/DidacticActivity';
import {IUser} from '../data-models/User';
import {UserType} from '../data-models/UserType';
import ITeacherActivity from '../data-models/TeacherActivity';

export default class DidacticActivityService {
  public static getActivities = async (
    user: IUser,
  ): Promise<IDidacticActivity[] | ITeacherActivity[]> => {
    const token = await AsyncStorage.getItem('token');

    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    let result;
    switch (user.roles[0].type) {
      default:
        throw new Error('Unknown type');
      case UserType.STUDENT:
      case UserType.TEACHER:
        result = await axios.post(
          `${config.apiUrl}/student/getActivities`,
          user,
        );
        break;
      case UserType.ADMINISTRATOR:
        throw new Error('ADMIN is not implemented1');
    }

    return result.data;
  };

  public static getActivitiesForChange = async (
    didactic: IDidacticActivity,
  ): Promise<IDidacticActivity[]> => {
    const token = await AsyncStorage.getItem('token');

    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    const result = await axios.post(
      `${config.apiUrl}/student/getActivitiesForChange`,
      didactic,
    );
    return result.data;
  };

  public static updateActivity = async (
    didactic: IDidacticActivity,
  ): Promise<IDidacticActivity> => {
    const token = await AsyncStorage.getItem('token');

    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    const result = await axios.post(
      `${config.apiUrl}/didacticActivity/update`,
      didactic,
    );
    return result.data;
  };

  public static getAllCourses = async (): Promise<IDidacticActivity[]> => {
    const token = await AsyncStorage.getItem('token');

    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    const result = await axios.get(
      `${config.apiUrl}/didacticActivity/getAllCourses`,
    );
    return result.data;
  };
}
