import axios from 'axios';

import {config} from '../config/config';
import AsyncStorage from '@react-native-community/async-storage';
import {IStudentDidacticRequest} from '../data-models/StudentDidacticRequest';

export default class StudentDidacticService {
  public static createActivity = async (
    request: IStudentDidacticRequest,
  ): Promise<void> => {
    const token = await AsyncStorage.getItem('token');

    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    const result = await axios.post(
      `${config.apiUrl}/student/createStudentActivity`,
      request,
    );
    return result.data;
  };
  public static deleteActivity = async (
    request: IStudentDidacticRequest,
  ): Promise<void> => {
    const token = await AsyncStorage.getItem('token');

    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    const result = await axios.post(
      `${config.apiUrl}/student/deleteStudentActivity`,
      request,
    );
    return result.data;
  };
}
