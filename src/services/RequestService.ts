import axios from 'axios';

import {config} from '../config/config';
import AsyncStorage from '@react-native-community/async-storage';
import IRequest from '../data-models/Request';

export default class RequestService {
  public static createRequest = async (request: IRequest): Promise<void> => {
    const token = await AsyncStorage.getItem('token');

    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    const result = await axios.post(
      `${config.apiUrl}/requests/createRequest`,
      request,
    );
    return result.data;
  };

  public static getRequests = async (id: number): Promise<IRequest[]> => {
    const token = await AsyncStorage.getItem('token');

    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    const result = await axios.get(
      `${config.apiUrl}/requests/getRequests?id=${id}`,
    );
    return result.data;
  };
}
