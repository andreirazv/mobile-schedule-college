# mobile-schedule-college

*Visual Studio Code* recommended extensions:
- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens) - for better git integration
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) - for code styling
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) - for code quality inspections

Some good practices:
- Don't delete `package-lock.json` and always commit it when there are changes to it (for example when a new package was installed). Its purpose is to make lock the version of the packages and avoid strange bugs caused by updates.
- From time to time (especially when starting work on a new branch/feature) delete the `node_modules` folder and run `npm install` inside the project root to make sure you have all the packages that are working for everybody else.
